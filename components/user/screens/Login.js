import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, Button } from 'react-native';

class Login extends Component {
  state = {
    email: "",
    password: "",
    submitted: false
  }

  render() {
    console.log("on login");
    return (
      <View style={styles.container}>
        <Text testID="login"> Login </Text>
        <TextInput
          testID="email"
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          onChangeText={(email) => this.setState({email})}
          value={this.state.email}
        />
        <TextInput
          testID="password"
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          onChangeText={(password) => this.setState({password})}
          value={this.state.password}
        />
        <Button testID='submit' title='Submit' onPress={() => this.setState({ submitted: true })}/>
        {this.state.submitted ? <Text testID='submitted'>success!</Text> : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  }
});

export default Login;