import React from 'react';
import {
  createStackNavigator,
  createAppContainer
} from 'react-navigation';


import Login from './components/user/screens/Login';
import SpotFeed from './components/home/screens/SpotFeed';

const AppNavigator = createStackNavigator({
  Login: {
    screen: Login
  },
  SpotFeed: {
    screen: SpotFeed
  }
});

const ContactContainer = createAppContainer(AppNavigator);

export default ContactContainer;