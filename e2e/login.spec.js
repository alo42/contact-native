describe('Logging in', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('should display the login screen', async () => {
    await expect(element(by.id('login'))).toBeVisible();
  });

  it('should redirect the user after succesful authentication', async () => {
    await element(by.id('email')).typeText('contact@example.com');
    await element(by.id('password')).typeText('P@ssword123!');
    await element(by.id('submit')).tap();

    await expect(element(by.id('spotFeed'))).toBeVisible();
  });

});